package view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

public class view extends JFrame {
	public view() {
		   createFrame();
		   
	   }
	   public void createFrame() {
		   showProjectName = new JLabel("Tokenizer");
		   showResults = new JTextArea("your resutls will be showed here");
		   endButton = new JButton("end program");
		   
		   setLayout(new BorderLayout());
		   add(showProjectName, BorderLayout.NORTH);
		   add(showResults,BorderLayout.CENTER);
		   add(endButton, BorderLayout.SOUTH);
		   
	   }
	   public void setProjectName(String s) {
		   showProjectName.setText(s);
	   }
	   public void setResult(String str) {
	       this.str = str;
		   showResults.setText(str);

	   }
	   public void extendResult(String str) {
		   this.str = this.str+"\n"+str;
		   showResults.setText(this.str);
	   }
	   public void setListener(ActionListener list) {
		   endButton.addActionListener(list);
	   }
	   private JLabel showProjectName;
	   private JTextArea showResults;
	   private JButton endButton;
	   private String str;
}
