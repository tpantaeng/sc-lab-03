package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import java.util.StringTokenizer;

import javax.swing.JOptionPane;

import model.model;
import view.view;

public class Control	{
	class ListenerMgr implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);
			
		}
		   
	   }
	public static void main(String[] args) {
		new Control();
	}
	public Control() {
		frame = new view();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(400,400);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}
	public void setTestCase() {
		String word1 = JOptionPane.showInputDialog("Enter your word");
		StringTokenizer word2 = new StringTokenizer(word1);
		StringTokenizer word3 = new StringTokenizer(word1);
		String len = JOptionPane.showInputDialog("Enter len of ngram");
		model x = new model();
		Integer count = x.countword(word2);
		frame.extendResult("word = "+word1);
		frame.extendResult("countword = "+count);
		int n = Integer.parseInt(len);
		String ng = x.ngram(word3,n);
		frame.extendResult("n = "+n);
		frame.extendResult("NGRAM = "+ng);
		
	}
	ActionListener list;
	view frame;
}
